'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
    NODE_ENV: '"development"',
    // API_HOST: '""', //本地接口请求前缀
    API_HOST: '""', //本地接口请求前缀
})