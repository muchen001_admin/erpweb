// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

Vue.prototype.$url = 'https://';
Vue.prototype.$imgUrl = 'https://cbu01.alicdn.com/';

import store from './vuex/index.js';
//axios安装好后即可导入
// import axios from 'axios'
// Vue.use(axios);
// import axios from './assets/js/axios';
import axios from './api/axios';
Vue.prototype.$axios = axios;
// import VueResource from 'vue-resource'
// Vue.use(VueResource)

// import VueJsonp from 'vue-jsonp';
// Vue.use(VueJsonp);
// import axios from '@/api/http';
// import axios from 'axios'
// const host = process.env.NODE_ENV === 'development' ? '; // 根据 process.env.NODE_ENV 的值判断当前是什么环境
//development本地环境  production线上环境
// const instance = axios.create({baseURL: host});
// Vue.prototype.$axios = instance;
import qs from 'qs';
Vue.prototype.qs = qs;




// import Tools from './components/js/tools'
// Vue.use(Tools)
// // 为了解决setItemEvent不立即生效，使用事件派发强制更高
// Tools.dispatchEventStroage()

/**
 * @description
 * @author (Set the text for this tag by adding docthis.authorName to your settings file.)
 * @date 2019-05-29
 * @param { number } type 1 localStorage 2 sessionStorage
 * @param { string } key 键
 * @param { string } data 要存储的数据
 * @returns
 */
Vue.prototype.$addStorageEvent = function(type, key, data) {
    if (type === 1) {
        // 创建一个StorageEvent事件
        var newStorageEvent = document.createEvent('StorageEvent');
        const storage = {
            setItem: function(k, val) {
                localStorage.setItem(k, val);
                // 初始化创建的事件
                newStorageEvent.initStorageEvent('setItem', false, false, k, null, val, null, null);
                // 派发对象
                window.dispatchEvent(newStorageEvent);
            }
        }
        return storage.setItem(key, data);
    } else {
        // 创建一个StorageEvent事件
        var newStorageEvent = document.createEvent('StorageEvent');
        const storage = {
            setItem: function(k, val) {
                sessionStorage.setItem(k, val);
                // 初始化创建的事件
                newStorageEvent.initStorageEvent('setItem', false, false, k, null, val, null, null);
                // 派发对象
                window.dispatchEvent(newStorageEvent);
            }
        }
        return storage.setItem(key, data);
    }
}
Vue.prototype.resetSetItem = function(key, newVal) {
    if (key === 'watchStorage') {
        // 创建一个StorageEvent事件
        var newStorageEvent = document.createEvent('StorageEvent');
        const storage = {
            setItem: function(k, val) {
                localStorage.setItem(k, val);
                // 初始化创建的事件
                newStorageEvent.initStorageEvent('setItem', false, false, k, null, val, null, null);
                // 派发对象
                window.dispatchEvent(newStorageEvent)
            }
        }
        return storage.setItem(key, newVal);
    }
}








// axios.defaults.headers.common['Authentication-Token'] = store.state.token;
//elementui安装好后即可导入
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);
//引入Echarts
import echarts from 'echarts';
Vue.prototype.$echarts = echarts;

//全局引入组件:nav
import Nav from '@/components/nav';
//全局注册组件
Vue.component('Nav', Nav);

Vue.config.productionTip = false;

//禁止单击遮罩层关闭对话框
ElementUI.Dialog.props.closeOnClickModal.default = false;

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    store,
    components: { App },
    template: '<App/>'
});