//操作数据源
export default {
  set_token(state,token) {
    state.token = token;
    sessionStorage.token = token
  },
  del_token(state) {
    state.token = '';
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('userInfo');
  },



  //初始化登录信息
  initLogin(state){
    state.loginStatus = true;
    state.token = localStorage.getItem('token');
  },

  //获取登录信息
  getUserInfo(state,newValue){
    state.userInfo = newValue;
    state.loginStatus = true;
    sessionStorage.userInfo = JSON.stringify(newValue);
    // localStorage.setItem('userInfo',JSON.stringify(newValue));
  },

  //安全退出
  logout(state){
    state.loginStatus = false;
    state.userInfo = {};
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('userInfo');
    // localStorage.removeItem('token');
  }
}
