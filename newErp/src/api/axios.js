import axios from 'axios';
import { MessageBox } from "element-ui";
import router from "@/router";
var root = process.env.API_HOST;
console.log('root', root);
// const axios = Axios.create({
// //production线上环境  development本地环境
//   baseURL:process.env.NODE_ENV === 'production'?'',
//
// });

//请求拦截
// axios.defaults.baseURL = '/erp';
axios.interceptors.request.use((config) => {
    // console.log('请求拦截器',config.url);
    if (sessionStorage.getItem('token') || config.url == '/Login/login' || config.url == '/Registered/sedMessaage' || config.url == '/Registered/adduser' || config.url == '/Registered/newsedMessaage' || config.url == '/Registered/chpassword' || config.url == '/Registered/checkusernamesam' || config.url == '/Registered/checkphoneuser') {
        //在发送请求之前做些什么
        // console.log('请求拦截器，正确处理');
        // console.log(Vue.$store)
        if (sessionStorage.getItem('token')) {
            // let token=sessionStorage.getItem('token');
            // config.headers.Authorization=token
        }
        //请求之前重新拼装url
        // config.url = "" + config.url;
        config.url = root + config.url;
        return config;
    } else {
        // console.log("没有tocken,请登录")
        MessageBox.alert('非法请求', '非法请求', {
            confirmButtonText: '确定',
            callback: action => {
                router.replace('/')
            }
        });
    }
    //请求之前重新拼装url
    //   config.url = root + config.url;
    return config;
});

var isNext = true;
//响应拦截器
axios.interceptors.response.use(function(response) {
    // console.log('响应拦截器，正确处理',response);
    if (isNext) {
        if (response.data.code == 2003) {
            MessageBox.alert('该账号已在另一端登录,请重新登录', '该账号已在另一端登录', {
                confirmButtonText: '确定',
                callback: action => {
                    sessionStorage.removeItem('token');
                    router.replace('/')
                        // router.replace({
                        //   name: '/',
                        //   // query: {redirect: router.currentRoute.fullPath} //登录后再跳回此页面时要做的配置
                        // })
                }
            });
            isNext = false;
            return false;
        } else if (response.data.code == 2004) {
            MessageBox.alert('登录超时,请重新登录', '登录超时', {
                confirmButtonText: '确定',
                callback: action => {
                    sessionStorage.removeItem('token');
                    router.replace('/')
                }
            });
            isNext = false;
            return false;
        } else if (response.data.code == 500) {
            // MessageBox.alert('非法请求,请重新登录后再试', '非法请求', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     sessionStorage.removeItem('token');
            //     router.replace('/')
            //   }
            // });
            // isNext = false;
            // return false;
            alert()
        }
    } else {
        isNext = true;
    }

    return response
}, function(error) {
    //请求错误时做些事
    //   console.log('响应拦截器，错误处理')
    //   console.log(error)
    loadinginstace.close();
    Message.error({
        message: '网络不给力,请稍后再试'
    })
    return Promise.reject(error)
});
export default axios;