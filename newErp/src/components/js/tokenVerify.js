import Axios from 'axios'

//请求拦截器
Axios.interceptors.request.use(function (config) {
//   console.log('请求拦截器，正确处理')
// console.log(Vue.$store)

  if(sessionStorage.getItem('token')){
    //在发送请求之前做些什么
    // console.log('请求拦截器，正确处理');
    // console.log(Vue.$store)
    // console.log(config)
    // console.log(sessionStorage.getItem('token'))
    let token=sessionStorage.getItem('token');
    config.headers.Authorization=token
  }else{
    // console.log("没有tocken,请登录")
  }

  return config


}, function (error) {
  //请求错误时做些事
  // console.log('请求拦截器，错误处理')
  // return Promise.reject(error)
})

//响应拦截器
Axios.interceptors.response.use(function (response) {
//对响应数据做些什么

  // console.log('响应拦截器，正确处理')
  // console.log(response)
  return response

}, function (error) {

//请求错误时做些事
//   console.log('响应拦截器，错误处理')
//   console.log(error)
  window.alert("未登录，请先去登录")
  // window.location.href="http://wx.ganyunnengyuan.com/console/index.html#/"
  return Promise.reject(error)
})

export default Axios
