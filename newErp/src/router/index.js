import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'

import store from '@/vuex/index'

Vue.use(Router);

let route = new Router({
    mode: 'hash',
    base: '/dist/', //添加的地方
    routes: [{
            path: '/helloWorld',
            name: 'HelloWorld',
            component: HelloWorld
        },
        {
            path: '/',
            name: 'Login',
            component: () =>
                import ('@/pages/login')
        },
        {
            path: '/register',
            name: 'Register',
            component: () =>
                import ('@/pages/register')
        },
        {
            path: '/forgetPassword',
            name: 'forgetPassword',
            component: () =>
                import ('@/pages/forgetPassword')
        },
        {
            path: '/index',
            name: 'Index',
            component: () =>
                import ('@/pages/index'),
            children: [{
                    path: '/',
                    name: 'shouye',
                    component: () =>
                        import ('@/pages/shouye')
                },
                {
                    path: 'adminManage',
                    name: 'adminManage',
                    component: () =>
                        import ('@/pages/system/adminManage')
                },
                {
                    path: 'roleManage',
                    name: 'roleManage',
                    component: () =>
                        import ('@/pages/system/roleManage')
                },
                {
                    path: 'shopManage',
                    name: 'shopManage',
                    component: () =>
                        import ('@/pages/system/shopManage')
                },
                {
                    path: 'productManage',
                    name: 'productManage',
                    component: () =>
                        import ('@/pages/product/productManage')
                },
                {
                    path: 'Releasegood',
                    name: 'Releasegood',
                    component: () =>
                        import ('@/pages/intelligent/Releasegood')
                },
                // {
                //     path: 'Releasegood',
                //     name: 'Releasegood',
                //     component: () =>
                //         import ('@/pages/product/Releasegood'),
                // },
                {
                    path: '/Releasegood1',
                    name: 'Releasegood1',
                    component: () =>
                        import ('@/pages/intelligent/Releasegood1')
                },
                {
                    path: '/Releasegood2',
                    name: 'Releasegood2',
                    component: () =>
                        import ('@/pages/intelligent/Releasegood2')
                },
                {
                    path: '/Releasegood3',
                    name: 'Releasegood3',
                    component: () =>
                        import ('@/pages/intelligent/Releasegood3')
                },
                {
                    path: 'issueProduct',
                    name: 'issueProduct',
                    component: () =>
                        import ('@/pages/product/issueProduct'),
                    children: [{
                            path: '/',
                            name: 'issueProduct2',
                            component: () =>
                                import ('@/pages/product/issueProduct2')
                        },
                        {
                            path: '/issueProduct3',
                            name: 'issueProduct3',
                            component: () =>
                                import ('@/pages/product/issueProduct3')
                        }
                    ]
                },

                {
                    path: 'issueSimpleProduct',
                    name: 'issueSimpleProduct',
                    component: () =>
                        import ('@/pages/product/issueSimpleProduct'),
                    children: [{
                            path: '/',
                            name: 'issueSimpleProduct2',
                            component: () =>
                                import ('@/pages/product/issueSimpleProduct2')
                        },
                        {
                            path: '/issueSimpleProduct3',
                            name: 'issueSimpleProduct3',
                            component: () =>
                                import ('@/pages/product/issueSimpleProduct3')
                        }
                    ]
                },
                { //智能抠图 
                    path: 'issueImg',
                    name: 'issueImg',
                    component: () =>
                        import ('@/pages/intelligent/issueImg'),
                    // children: []
                },
                {
                    path: 'UploadComplete',
                    name: 'UploadComplete',
                    component: () =>
                        import ('@/pages/intelligent/UploadComplete')
                },
                { //视频生成
                    path: 'issueVideo',
                    name: 'issueVideo',
                    component: () =>
                        import ('@/pages/intelligent/issueVideo'),
                },
                // { //自动评论
                //     path: 'evaluation',
                //     name: 'evaluation',
                //     component: () =>
                //         import ('@/pages/product/evaluation'),
                // },
                {
                    path: 'myShop',
                    name: 'myShop',
                    component: () =>
                        import ('@/pages/system/myShop')
                },
                {
                    path: 'addShop',
                    name: 'addShop',
                    component: () =>
                        import ('@/pages/system/addShop')
                },
                {
                    path: 'clientData',
                    name: 'clientData',
                    component: () =>
                        import ('@/pages/client/clientData')
                },
                {
                    path: 'cashierList',
                    name: 'cashierList',
                    component: () =>
                        import ('@/pages/cashier/cashierList'),
                    children: [{
                            path: '/',
                            name: 'index',
                            component: () =>
                                import ('@/pages/cashier/index')
                        },
                        {
                            path: '/list',
                            name: 'list',
                            component: () =>
                                import ('@/pages/cashier/list')
                        },
                        {
                            path: '/listDetail',
                            name: 'listDetail',
                            component: () =>
                                import ('@/pages/cashier/listDetail')
                        }
                    ]
                },
                {
                    path: 'financeList',
                    name: 'financeList',
                    component: () =>
                        import ('@/pages/finance/financeList')
                },
                {
                    path: 'logistics',
                    name: 'logistics',
                    component: () =>
                        import ('@/pages/logistics/logistics')
                },
                {
                    path: 'authorization',
                    name: 'authorization',
                    component: () =>
                        import ('@/pages/logistics/authorization')
                },
                { //优化引流--排名查询
                    path: 'ranking',
                    name: 'ranking',
                    component: () =>
                        import ('@/pages/drainage/ranking')
                },
                { //优化引流--标题优化
                    path: 'titleOptimize',
                    name: 'titleOptimize',
                    component: () =>
                        import ('@/pages/drainage/titleOptimize')
                },
                { //优化引流--违禁词检测
                    path: 'violateWordTest',
                    name: 'violateWordTest',
                    component: () =>
                        import ('@/pages/drainage/violateWordTest')
                },
                { //优化引流--自动补库存
                    path: 'repertory',
                    name: 'repertory',
                    component: () =>
                        import ('@/pages/drainage/repertory')

                }
            ]
        }
    ]
});



const VueRouterPush = Router.prototype.push;
Router.prototype.push = function push(to) {
    return VueRouterPush.call(this, to).catch(err => err)
}

// 页面刷新时，重新赋值token
if (sessionStorage.getItem('token')) {
    store.commit('getUserInfo', JSON.parse(sessionStorage.getItem('userInfo')));
    store.commit('set_token', sessionStorage.getItem('token'));
}

route.beforeEach((to, from, next) => {
    //这里的requireAuth为路由中定义的 meta:{requireAuth:true}，意思为：该路由添加该字段，表示进入该路由需要登陆的
    if (to.matched.some(r => r.meta.requireAuth)) {
        if (store.state.token) {
            next();
        } else {
            next({ path: '/login', query: { redirect: to.fullPath } })
        }
    } else { next(); }
});








// route.beforeEach((to, from, next) => {
//   if (to.path === '/login') {
//     next();
//   } else {
//     let token = sessionStorage.getItem('token');
//     if (token === null || token === '') {
//       next('/');
//     } else {
//       next();
//     }
//   }
// });
// if (sessionStorage.getItem('token')) {
//   store.commit('set_token', sessionStorage.getItem('token'))
// }



export default route;